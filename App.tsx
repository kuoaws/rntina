/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { type PropsWithChildren } from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import HomeScreen from './screen/HomeScreen';
import CategoryScreen from './screen/CategoryScreen';
import ProductDetailScreen from './screen/ProductDetailScreen';

import { RootStackParamList } from './models/navigation'

const RootStack = createNativeStackNavigator<RootStackParamList>();

const App = () => {
  // const isDarkMode = useColorScheme() === 'dark';
  const isDarkMode = false;

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <NavigationContainer>
        <RootStack.Navigator screenOptions={{
          headerStyle: { backgroundColor: '#351401' },
          headerTintColor: 'white',
          contentStyle: { backgroundColor: '#f2803f' }
        }}>
          <RootStack.Screen
            name='Home'
            component={HomeScreen}
            options={{ title: 'All Categories' }} />

          <RootStack.Screen
            name='Category'
            component={CategoryScreen}
          // options={({ route, navigation }) => {
          //   return {
          //     title: route.params.id
          //   }
          // }}
          />

          <RootStack.Screen
            name='ProductDetail'
            component={ProductDetailScreen}
          // options={
          //   {
          //     headerRight: () => <Button title='tap me!' />
          //   }
          // } 
          />

        </RootStack.Navigator>
      </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
