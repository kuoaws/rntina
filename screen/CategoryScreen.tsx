import { StyleSheet, Text, View, FlatList } from 'react-native'
import React, { useEffect, useLayoutEffect } from 'react'
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../models/navigation'
import { MEALS, CATEGORIES } from '../data/dummy-data'
import ProductOverview from '../components/ProductOverview'

type ScreenProps = NativeStackScreenProps<RootStackParamList, 'Category'>

const CategoryScreen = ({ route, navigation }: ScreenProps) => {
    const categoryId = route.params.id;
    const displayMeals = MEALS.filter(meal => meal.categoryIds.includes(categoryId));

    useLayoutEffect(() => {
        const categoryTitle = CATEGORIES.find(x => x.id === categoryId)?.title;
        navigation.setOptions({
            title: categoryTitle
        });
    }, [])

    return (
        <View style={styles.container}>
            <FlatList
                data={displayMeals}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => <ProductOverview {...item} />} />
        </View>
    )
}

export default CategoryScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16
    }
})