import { Image, StyleSheet, Text, View, ScrollView, Button } from 'react-native'
import React, { useLayoutEffect } from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../models/navigation'
import { MEALS } from '../data/dummy-data'
import ProductDescription from '../components/ProductDescription'
import IconButton from '../components/IconButton'

type ScreenProps = NativeStackScreenProps<RootStackParamList, 'ProductDetail'>

const ProductDetailScreen = ({ route, navigation }: ScreenProps) => {
  const { id } = route.params;
  const meal = MEALS.find(x => x.id === id);

  const handlePress = () => {

  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => <IconButton icon='star' color='white' onPress={handlePress} />
    });
  }, [])

  return (
    <ScrollView style={{ marginBottom: 32 }}>
      <Image style={styles.image} source={{ uri: meal?.imageUrl }} />
      <Text style={styles.title}>{meal?.title}</Text>
      {meal && <ProductDescription {...meal} textStyle={styles.text} />}

      <View style={styles.listOuterContainer}>
        <View style={styles.listContainer}>
          <View style={styles.subTitleContainer}>
            <Text style={styles.subTitle}>Ingredients</Text>
          </View>
          {meal && meal.ingredients.map(x => <Text style={{ textAlign: 'center' }} key={x}>{x}</Text>)}
          <View style={styles.subTitleContainer}>
            <Text style={styles.subTitle}>Step</Text>
          </View>
          {meal && meal.steps.map(x => <Text style={{ textAlign: 'center' }} key={x}>{x}</Text>)}
        </View>

      </View>
    </ScrollView>
  )
}

export default ProductDetailScreen

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 350
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    margin: 8,
    textAlign: 'center',
    color: 'white'
  },
  subTitleContainer: {
    borderBottomColor: 'white',
    borderBottomWidth: 2,
    padding: 8,
    marginHorizontal: 24,
    marginVertical: 4
  },
  subTitle: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',

  },
  text: {
    color: 'white'
  },
  listOuterContainer: {
    alignItems: 'center'
  },
  listContainer: {
    width: '80%'
  }
})