import { FlatList, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

import { CATEGORIES } from '../data/dummy-data'
import CategoryGridTile from '../components/CategoryGridTile'
import { RootStackParamList } from '../models/navigation'

type ScreenProps = NativeStackScreenProps<RootStackParamList, 'Home'>

const HomeScreen = ({ navigation, route }: ScreenProps) => {

    const handlePress = (categoryId: string) => {
        navigation.navigate('Category', { id: categoryId })
    }

    return (
        <FlatList
            data={CATEGORIES}
            keyExtractor={item => item.id}
            renderItem={({ item }) => <CategoryGridTile {...item} onPress={handlePress} />}
            numColumns={2}
        />
    )
}

export default HomeScreen

const styles = StyleSheet.create({})