import { StyleSheet, Text, View, Pressable, Image } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { RootStackParamList } from '../models/navigation'
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import ProductDescription from '../components/ProductDescription'

interface Props {
    id: string;
    title: string;
    imageUrl: string;
    duration: number;
    complexity: string;
    affordability: string;
}

type navigationProp = NativeStackNavigationProp<RootStackParamList>;

const ProductOverview = ({ id, title, imageUrl, duration, complexity, affordability }: Props) => {
    const navigation = useNavigation<navigationProp>();

    const handlePress = () => {
        navigation.navigate("ProductDetail", { id })
    }

    return (
        <View style={styles.item}>
            <Pressable
                style={({ pressed }) => pressed ? { opacity: 0.5 } : null}
                android_ripple={{ color: '#ccc' }}
                onPress={handlePress}>
                <View>
                    <Image source={{ uri: imageUrl }} style={styles.image} />
                    <Text style={styles.title}>{title}</Text>
                </View>
                <ProductDescription duration={duration} complexity={complexity} affordability={affordability} />
            </Pressable>
        </View>
    )
}

export default ProductOverview

const styles = StyleSheet.create({
    item: {
        margin: 16,
        borderRadius: 8,
        overflow: 'hidden',
        backgroundColor: 'white',
        elevation: 8
    },
    image: {
        width: '100%',
        height: 200,
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
        margin: 8
    }
})