import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

interface Props {
    duration: number;
    complexity: string;
    affordability: string;
    style?: Object
    textStyle?: Object
}

const ProductDescription = ({ duration, complexity, affordability, style, textStyle }: Props) => {
    return (
        <View style={[styles.container, style]}>
            <Text style={[styles.description, textStyle]}>{duration}m</Text>
            <Text style={[styles.description, textStyle]}>{complexity.toUpperCase()}</Text>
            <Text style={[styles.description, textStyle]}>{affordability.toUpperCase()}</Text>
        </View >
    )
}

export default ProductDescription

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 8
    },
    description: {
        marginHorizontal: 4,
        fontSize: 12
    }
})