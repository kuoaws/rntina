import { StyleSheet, Text, View, Pressable } from 'react-native'
import React from 'react'

interface Props {
    id: string;
    title: string;
    color: string;
    onPress: (id: string) => void;
}

const CategoryGridTile = ({ id, title, color, onPress }: Props) => {

    return (
        <View style={[styles.container, { backgroundColor: color }]}>
            <Pressable
                style={({ pressed }) => [styles.button, pressed ? styles.buttonPressed : null]}
                android_ripple={{ color: '#ccc' }}
                onPress={onPress.bind(this, id)}
            >
                <View style={[styles.item]}>
                    <Text style={styles.title}>{title}</Text>
                </View>
            </Pressable>
        </View>
    )
}

export default CategoryGridTile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 16,
        height: 150,
        borderRadius: 16,
        elevation: 4,
        overflow: 'hidden'
    },
    item: {
        flex: 1,
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18
    },
    button: {
        flex: 1,
    },
    buttonPressed: {
        opacity: 0.5
    }
})