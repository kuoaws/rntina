export type RootStackParamList = {
    Home: undefined;
    Category: { id: string };
    ProductDetail: { id: string };
}